---
title: 'Extracurricular Experience'
date: 2024-10-12T16:47:09-03:00
draft: false
type: "atv_ext"
nome: "Poli Racing Team" 
cargo: "Finance Director and Marketing Director"
data: "jun. 2022 -  present"
descricao: 
    - "Competitive group composed of 60 students from the Polytechnic School, responsible for the annual construction of a Formula SAE prototype. I actively manage 11 members and have been responsible for securing over R$200,000 in sponsorships."
    - "Technical skills: project management, sponsorship acquisition and management, budgeting and scheduling, expense control and preparation of reports for sponsors, graphic design and social media management, 3D modeling and rendering for creating the car's visual identity (livery)."
    - "Soft skills: leadership and teamwork; effective communication and negotiation of sponsorships and partnerships; strategic planning, innovation, and creativity."
    - "Team trajectory: Marketing Member, Specialist Designer, Sponsorship Manager, Driver, Finance Director and Marketing Director."
    - "Tools utilized: Excel, Canva, Blender, Inkscape."

nome2: "Development of  autonomous navigation software based on ROS 2 for an agricultural robot" 
cargo2: "Scientific Initiation  - Advanced Perception Lab Poli USP"
data2: "sept. 2023 -  aug. 2024"
descricao2: 
    - "Implementation of a ROS 2 framework for autonomous navigation of an agricultural robot from Agrobotz, using image recognition with neural networks (YOLOv8)."
    - "Technical skills: advanced simulation with Gazebo and RViz2, implementation of control and navigation algorithms for autonomous robots, development and testing of artificial intelligence algorithms, optimization of the system to ensure real-time efficiency."
    - "Soft skills: critical thinking and effective problem-solving, clear and assertive communication, resilience and focus on project completion."
    - "Tools utilized: ROS2, YOLOv8, Python, OpenCV."

---
