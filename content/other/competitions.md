---
title: 'Competitions and distinctions'
date: 2024-10-12T16:47:09-03:00
draft: false
type: "competitions"

nome: "Class representative" 
cargo: "POLI - USP"
data: "mar. 2022 - present"
descricao: 
    - "Elected class representative in my first year at Poli, acting as a communication liaison between students and faculty, organizing class activities, and representing classmates' concerns and suggestions."

nome2: "20th National Formula SAE Competition" 
cargo2: "Poli Racing Team"
data2: "aug. 2024"
descricao2: 
    - "19th place overall and 8th place in Management presentation. I participated in the competition as one of the 20 representatives from Poli Racing and was the driver during the dynamic events of the competition."

nome3: "Brazilian Astronomy and Astronautics Olympiad (OBA)" 
cargo3: "Gold medal"
data3: "oct. 2021"
descricao3: 
    - "National Olympiad that tests knowledge in Science, Physics, and Geography"

nome4: "Honors English Program" 
cargo4: "Colégio Bandeirantes"
data4: "feb. 2018 - dec. 2021"
descricao4: 
    - "Program aimed at students with advanced English skills, offering a challenging curriculum focused on the critical development of reading, writing, and speaking in English."

---