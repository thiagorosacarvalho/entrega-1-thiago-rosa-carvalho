---
title: 'Voluntary Activities'
date: 2024-10-12T16:47:09-03:00
draft: false
type: "voluntary"
nome: "ARPA (Classes and summaries for students)" 
cargo: "Chemistry Coordinator and Math Tutor"
data: "feb. 2019 - dec. 2021"
descricao: 
    - "Student organization that provided free tutoring and class reviews every semester."
    - "Team Coordination: direct management of 5 volunteers as Chemistry Coordinator."
    - "Skill Development: team guidance, time management, and teaching."
    - "Responsibilities: teaching and preparing summaries; guiding subject teams; ensuring timely delivery of materials; overseeing project organization, including scheduling volunteer classes, maintaining communication with teachers for guidance and feedback and editing summaries"

---