---
title: 'Professional experience'
date: 2024-10-12T16:47:09-03:00
draft: false
type: "atv_prof"
nome: "NoonApp - University Project" 
cargo: "Tech builder (co-founder)"
data: "jun. 2024 - present"
descricao: 
    - "Development of features using artificial intelligence (AI) for the innovative UP platform by NoonApp, which empowers users to manage their identity and career. The platform employs decentralized identity for secure data management and AI to build profiles, create personalized career paths, and develop professional networks. Additionally, it promotes a tokenized economy to reward contributions within user communities."
    - "Participated in a technical team composed of 5 students from universities in Brazil and the USA."
    - "Technical Skills: development of AI-based solutions, integration of Large Language Models (LLM - Ollama) with applications through APIs, customization and tuning of prompts to maximize the accuracy and relevance of LLM responses."
    - "Soft skills: time management and autonomy, adaptability, daily and weekly results presentation, clarity and objectivity in English communication."
    - "Tools utilized: Python, VScode, JSON, Flask APIs, Postman, Miro."


---