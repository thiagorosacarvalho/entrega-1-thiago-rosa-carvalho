---
type: "home"
title: "Welcome to my curricullum vitae site!"
subtitle: "Explore my academic achievements, technical expertise, and contributions to projects inside and outside my university"

aboutme: "About me"
text: "I am a person with diverse interests and a great passion for what I do. I love practicing and watching sports, especially those involving motors and martial arts, such as Formula 1, MotoGP, and UFC. I highly value time spent with my family and appreciate gastronomy. I am a culinary enthusiast and enjoy exploring new dining places with my parents, grandparents, and friends."

info:
    - "- Date of birth: 31/03/2004"
    - "- Phone: +55 11 98837-4861"
    - "- USP email: thiagorosacarvalho@usp.br"
    - "- Alternative email: thi.rscv@gmail.com"
    - "- Adress: Rua Corinto 431, 124b, CEP: 05586-060, São Paulo, Sp"

---